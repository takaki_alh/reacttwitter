import React, {Component} from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {Link} from 'react-router-dom'
import 'whatwg-fetch';
import * as actions from './action/actions';
export class PostPage_Comp extends Component {
  constructor(props) {
    super(props)
    const {dispatch} = props;
    this.action = bindActionCreators(actions, dispatch);
    this.state = {
      title: '',
      text: ''
    }
  }
  render() {
    const handleChaged = (e) => {
      this.setState({[e.target.name]: e.target.value})
    }

    const sendButtonClicked = () => {
      confirm("送信してもよろしいですか?") &&
      postSend()
    }
    const postSend = () => {
      // stateの値をJSONに整形
      const data = {title: this.state.title, text: this.state.text}
      // POST送信実行
      fetch("http://localhost:8080/twitter/getTweet", {
        method: "POST",
        body: JSON.stringify(data)
      })
      // レスポンス返却時の処理
      .then((response) => {
        if(response.status === 200) {
          this.props.history.push("/")
        } else {
          alert("送信に失敗しました。ステータス")
        }
      })
      .catch(error => console.error(error));
    }
    return(
      <div>
      <h1>投稿ページ</h1>
      <div className="postArea">
      <p>タイトル</p>
      <input type="text" name="title" onChange={handleChaged}></input>
      <p>本文</p>
      <textarea rows={3} name="text" onChange={handleChaged}></textarea>
      </div>
      <button onClick={sendButtonClicked}>送信する</button>
      <p><Link to="/">トップへ戻る</Link></p>
      </div>
    )
  }
}
PostPage_Comp.propTypes = {
  dispatch: PropTypes.func,
  history: PropTypes.object
}
function mapStateToProps(state) {
  return state
}
export const PostPage = withRouter(connect(mapStateToProps)(PostPage_Comp))
