import React, {Component} from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {Link} from 'react-router-dom'
import * as actions from './action/actions';
class TopPage_Comp extends Component{
  constructor(props) {
    super(props)
    const {dispatch} = props;
    this.action = bindActionCreators(actions, dispatch);
  }

  componentDidMount() {
    fetch("http://localhost:8080/messages", {
      method: "GET"
    })
    .then((response) => {
      response.json().
      then(json => {
        this.action.updateTweet(json.title, json.text)
      })
    })
  }
  render() {
    return(
      <div>
      <h1>トップページ</h1>
      <p>タイトル:{this.props.tweet.title}</p>
      <p>本文:{this.props.tweet.text}</p>
      <p><Link to="/post">投稿する</Link></p>
      </div>
    )
  }
}
TopPage_Comp.propTypes = {
  dispatch: PropTypes.func,
  name: PropTypes.string,
  tweet: PropTypes.object
}
function mapStateToProps(state) {
  return state
}
export const TopPage = withRouter(connect(mapStateToProps)(TopPage_Comp))
