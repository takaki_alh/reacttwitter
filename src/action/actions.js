export function updateName(name) {
  return {
    type: 'UPDATE_NAME',
    name: name
  }
}

export function updateTweet(title, text) {
  return {
    type: 'UPDATE_TWEET',
    title: title,
    text: text
  }
}
